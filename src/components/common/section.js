import React from 'react'
import { Container } from 'react-bootstrap'
import styles from './styles/section.module.css'

export const Section = (props) => (

    

    <Container id={ props.id } fluid style={{ backgroundColor: props.bgColor }} className={ styles.section }>
       
        {props.children}
     
    </Container>

)