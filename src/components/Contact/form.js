import React from 'react'
import { Form, Button } from 'react-bootstrap'
import { Formik, ErrorMessage } from 'formik'
import * as Yup from 'yup'

export const ContactForm = () => (

    <Formik 
        initialValues={{ name: "", email: "", phone: "", message: "" }}
        validationSchema={Yup.object().shape({
            name: Yup.string().required("Required"),
            email: Yup.string().email("Must be a valid e-mail address").required("Required"),
            phone: Yup.string().required("Required"),
            message: Yup.string().required("Required")
        })}
    >
        { props => {

            const {
                values,
                isSubmitting,
                handleChange,
                handleBlur
            } = props;

            return (

                <Form method="POST" action="https://getform.io/f/455e3df4-a07f-48ff-8f14-288a1a45a67c">
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} />
                        <ErrorMessage name="name" component="div" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>E-mail Address</Form.Label>
                        <Form.Control type="email" name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} />
                        <ErrorMessage name="email" component="div" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Phone Number</Form.Label>
                        <Form.Control type="phone" name="phone" onChange={handleChange} onBlur={handleBlur} value={values.phone} />
                        <ErrorMessage name="phone" component="div" />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Message</Form.Label>
                        <Form.Control as="textarea" name="message" rows="3" onChange={handleChange} onBlur={handleBlur} value={values.message} />
                        <ErrorMessage name="message" component="div" />
                    </Form.Group>
                    <Button block variant="primary" size="lg" type="submit" disabled={isSubmitting}>Hollar!</Button>
                </Form>

            );
        }}
    </Formik>
    
)