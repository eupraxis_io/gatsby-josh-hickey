import React from 'react'
import { Container, Col, Row, Jumbotron } from 'react-bootstrap'
import Styled from 'styled-components'
import styled from 'styled-components'

export const Hero = () => (

    <Container fluid style={{ padding: "3rem 0", backgroundImage: "url('https://via.placeholder.com/300x800')"}}>
        <Container>
            <Row>
                <Col>
                    <Jumbotron>
                        <h1>Hero.</h1>
                        <p>Wow. So amazing.</p>
                    </Jumbotron>
                    
                </Col>
            </Row>
        </Container>
    </Container>
    
)
