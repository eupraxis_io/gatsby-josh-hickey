import React from "react"

import { Layout }  from "../components/Layout/layout"
import { HeroSplit } from "../components/HeroSplit/herosplit"
import { Projects } from "../components/Projects/projects"
import { Contact } from "..//components/Contact/contact"
import { Skills } from "../components/Skills/skills"
import { Analytics } from "../components/common/analytics"
import { Analytics2 } from "../components/common/analytics2"

const IndexPage = () => (
  <>
  <Layout>
    <HeroSplit />
    <Projects />
    <Contact />
    <Skills />
  </Layout>
  <Analytics />
  <Analytics2 />
</>
)

export default IndexPage
